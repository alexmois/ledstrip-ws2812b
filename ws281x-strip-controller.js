"use strict";

var ws281x = require('rpi-ws281x-native');

module.exports = WS281XService;

function WS281XService (length,options){
    this.length = length;
    this.options = options;
    ws281x.init(length,options);
    process.on('SIGINT', function() {
        ws281x.reset();
        process.nextTick(exit);
    });
}

WS281XService.prototype.send = function(buffer){
    var colorData = new Uint32Array(30);
    for (var i=0; i<30; i++) {
        var r = buffer[0]||0, g = buffer[1]||0, b = buffer[2]||0;
        colorData[i] = ((buffer[0] & 0xff) << 16) + ((buffer[1] & 0xff) << 8) + (buffer[2] & 0xff);
        
	//colorData[i] = rgb2Int(255, 0, 0);
		
    }
    ws281x.render(colorData);
}

function exit(){
    process.exit(0)
}
